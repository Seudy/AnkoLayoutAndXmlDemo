package com.example.seudy.ankolayoutdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.example.seudy.ankolayoutdemo.adpater.AnkoLayoutAdapter
import com.example.seudy.ankolayoutdemo.model.Employee
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

class RecyclerViewAnkoLayoutActivity : AppCompatActivity() {
    lateinit var recycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        linearLayout {
            recycler = recyclerView {
                lparams(width = matchParent, height = wrapContent) { }
                layoutManager = LinearLayoutManager(this@RecyclerViewAnkoLayoutActivity)
                adapter = AnkoLayoutAdapter(intent.getParcelableArrayListExtra("LIST_EMP"))

            }
        }
        //recycler.scrollToPosition((intent.getParcelableArrayListExtra<Employee>("LIST_EMP")).size -1)
    }
}
