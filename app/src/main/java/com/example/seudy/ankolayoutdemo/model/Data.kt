package com.example.seudy.ankolayoutdemo.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by SAMBO on 20-Jun-17.
 */
data class Employee(var id: Int, var name: String, var gender: String, var department: String, var position: String): Parcelable{
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(id)
        dest.writeString(name)
        dest.writeString(gender)
        dest.writeString(department)
        dest.writeString(position)
    }

    override fun describeContents(): Int {
        return 0
    }


    private constructor(p: Parcel) : this(p.readInt(), p.readString(), p.readString(), p.readString(), p.readString())

    companion object{
        @JvmField val CREATOR = object : Parcelable.Creator<Employee>{
            override fun createFromParcel(source: Parcel): Employee {
                return Employee(source)
            }

            override fun newArray(size: Int): Array<Employee?> {
                return arrayOfNulls(size)
            }
        }
    }


}