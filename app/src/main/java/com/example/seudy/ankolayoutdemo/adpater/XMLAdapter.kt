package com.example.seudy.ankolayoutdemo.adpater

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.seudy.ankolayoutdemo.R
import com.example.seudy.ankolayoutdemo.model.Employee
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item.*
import kotlinx.android.synthetic.main.item.view.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

/**
 * Created by SAMBO on 20-Jun-17.
 */
class XMLAdapter(var empList: List<Employee>): RecyclerView.Adapter<XMLAdapter.ViewHolder>(){
    var timeRecords = ArrayList<Long>()

    override fun getItemCount(): Int {
        return empList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindEmployee(empList[position])

        if(position == empList.size-1){
            val min = timeRecords.min()
            val max = timeRecords.max()
            val avg = timeRecords.average()
            holder.itemView.context.longToast("XML Layout: min = $min, max = $max, avg = $avg")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val now = System.currentTimeMillis()
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        timeRecords.add(System.currentTimeMillis() - now)
        return  ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bindEmployee(emp: Employee){
            with(emp){
                Picasso.with(itemView.context).load(if(emp.gender.equals("Male"))R.drawable.profile_male else R.drawable.profile_female).into(itemView.profile)
                itemView.tv_name.text = emp.name
                itemView.tv_depart.text = emp.department
                itemView.tv_position.text = emp.position
            }
        }
    }
}