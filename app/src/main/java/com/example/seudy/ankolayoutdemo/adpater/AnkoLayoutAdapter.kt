package com.example.seudy.ankolayoutdemo.adpater

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.seudy.ankolayoutdemo.R
import com.example.seudy.ankolayoutdemo.extension.bold
import com.example.seudy.ankolayoutdemo.model.Employee
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item.view.*
import org.jetbrains.anko.*
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

/**
 * Created by SAMBO on 20-Jun-17.
 */
class AnkoLayoutAdapter(var empList: List<Employee>) : RecyclerView.Adapter<AnkoLayoutAdapter.ViewHolder>() {
    var timeRecords = ArrayList<Long>(10)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindEmp(empList[position])

        if(position == empList.size-1){
            val min = timeRecords.min()
            val max = timeRecords.max()
            val avg = timeRecords.average()
            holder.itemView.context.longToast("Anko Layout: min = $min, max = $max, avg = $avg")
        }
    }

    override fun getItemCount(): Int {
        return empList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val now = System.currentTimeMillis()
        val view = AnkoLayoutItem().createView(AnkoContext.Companion.create(parent.context, parent))
        timeRecords.add(System.currentTimeMillis() - now)
        return ViewHolder(view)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindEmp(emp: Employee) = with(emp) {
            itemView.tv_name.text = name
            itemView.tv_depart.text = department
            itemView.tv_position.text = position
            Picasso.with(itemView.context).load(if (gender.equals("Male")) R.drawable.profile_male else R.drawable.profile_female).into(itemView.profile)
        }
    }

    class AnkoLayoutItem : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                linearLayout {
                    orientation = LinearLayout.HORIZONTAL
                    lparams(width = matchParent, height = dip(100)) {
                        gravity = Gravity.CENTER_VERTICAL
                        imageView {
                            id = R.id.profile
                        }.lparams {
                            width = dip(80)
                            height = dip(80)
                            leftMargin = dip(16)
                        }

                        linearLayout {
                            orientation = LinearLayout.VERTICAL
                            linearLayout {
                                orientation = LinearLayout.HORIZONTAL
                                textView {
                                    text = "Name: "
                                    typeface = bold
                                }
                                textView {
                                    id = R.id.tv_name
                                }
                            }.lparams {
                                width = matchParent
                            }

                            linearLayout {
                                orientation = LinearLayout.HORIZONTAL
                                textView {
                                    text = "Department: "
                                    typeface = bold
                                }
                                textView {
                                    id = R.id.tv_depart
                                }
                            }.lparams {
                                width = matchParent
                                topMargin = dip(10)
                            }

                            linearLayout {
                                orientation = LinearLayout.HORIZONTAL
                                textView {
                                    text = "Position: "
                                    typeface = bold
                                }
                                textView {
                                    id = R.id.tv_position
                                }
                            }.lparams {
                                width = matchParent
                                topMargin = dip(10)
                            }
                        }.lparams {
                            width = matchParent
                            horizontalMargin = dip(16)
                        }
                    }
                }
            }
        }
    }
}