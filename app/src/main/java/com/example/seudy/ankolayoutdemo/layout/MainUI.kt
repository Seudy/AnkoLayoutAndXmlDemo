package com.example.seudy.ankolayoutdemo.layout

import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import com.example.seudy.ankolayoutdemo.MainActivity
import com.example.seudy.ankolayoutdemo.R
import com.example.seudy.ankolayoutdemo.RecyclerViewAnkoLayoutActivity
import com.example.seudy.ankolayoutdemo.RecyclerViewXMLActivity
import com.example.seudy.ankolayoutdemo.model.Employee
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Created by SAMBO on 20-Jun-17.
 */
class MainUI : AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {

        var listEmp = listOf<Employee>(
                                Employee(1, "Jonh Legend", "Male", "Smart", "Manager"),
                                Employee(2, "Katty Perry", "Female", "Design", "Designer"),
                                Employee(3, "Justin Beiber", "Male", "Network", "Team Leader"),
                                Employee(4, "Taylor Swift", "Female", "Web", "Team Leader"),
                                Employee(5, "Ariana Grande", "Female", "Smart", "Developer"),
                                Employee(6, "Justin Timberlake", "Male", "Network", "Manager")/*,
                                Employee(7, "Ed Sheeran", "Male", "Smart", "Team Leader"),
                                Employee(8, "Selena Gomez", "Female", "Design", "Team Leader"),
                                Employee(9, "Charlie Putt", "Male", "Web", "Developer"),
                                Employee(10, "Bruno Mars", "Male", "Design", "Manager")*/)
        linearLayout {
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER
            backgroundColor = 0xffffff.opaque
            button("XML Layout") {
                backgroundColor = 0x64B5F6.opaque
                onClick {
                    startActivity<RecyclerViewXMLActivity>("LIST_EMP" to listEmp)
                }
            }.lparams {
                width = matchParent
                horizontalMargin = dip(16)
            }
            button("Anko Layout") {
                backgroundColor = 0xFF80AB.opaque
                onClick {
                    startActivity<RecyclerViewAnkoLayoutActivity>("LIST_EMP" to listEmp)
                }
            }.lparams {
                width = matchParent
                topMargin = dip(16)
                horizontalMargin = dip(16)
            }
        }.applyRecursively {
            view ->
            when (view) {
                is Button -> {
                    view.textSize = 14f
                    view.textColor = 0xffffff.opaque
                }
            }
        }
    }
}