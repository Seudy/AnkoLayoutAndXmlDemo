package com.example.seudy.ankolayoutdemo.extension

import android.graphics.Typeface
import android.widget.TextView

/**
 * Created by SAMBO on 21-Jun-17.
 */
val TextView.bold: Typeface get() = Typeface.DEFAULT_BOLD
//val TextView.bold: Typeface get() = Typeface.createFromAsset(context.assets, "fonts/Roboto-Bold.ttf")