package com.example.seudy.ankolayoutdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.seudy.ankolayoutdemo.adpater.XMLAdapter
import com.example.seudy.ankolayoutdemo.model.Employee
import kotlinx.android.synthetic.main.activity_recycler_view_xml.*

class RecyclerViewXMLActivity : AppCompatActivity() {
    lateinit var adapter: XMLAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view_xml)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter  = XMLAdapter(intent.getParcelableArrayListExtra("LIST_EMP"))
        //recycler_view.scrollToPosition(intent.getParcelableArrayListExtra<Employee>("LIST_EMP").size-1)

    }
}
